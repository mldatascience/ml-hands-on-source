from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, roc_curve, precision_recall_curve, confusion_matrix
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler
from sklearn.tree import export_graphviz
import os

import pandas as pd
import numpy as np

train_df = pd.read_csv('./data/trainingset.csv')

def take_dataset(df,sampling_size):
#    working_columns=['TARGET','NAME_CONTRACT_TYPE','FLAG_OWN_CAR','AMT_CREDIT','CNT_CHILDREN','CODE_GENDER','FLAG_OWN_REALTY']
#    df=df[working_columns]
#    df['FLAG_OWN_CAR']=np.where(df['FLAG_OWN_CAR']=='y',1,0)
    df=df.sample(n=sampling_size)
    #preprocessing
    categorical_feats = df.columns[df.dtypes == 'object']
    for feat in categorical_feats:
        encoder = LabelEncoder()
        df[feat] = encoder.fit_transform(df[feat].fillna('NULL'))
    y=df.pop('TARGET')
    df.fillna(0, inplace=True)
    return df, y




n=100000 #number of sample to use
X, y=take_dataset(train_df, n)


clf = RandomForestClassifier(verbose=1,max_depth=100, n_estimators=1000, random_state=0)

clf.fit(X, y)
#print(clf.feature_importances_)

#load test data to predict
PredictN=10000
test_df = pd.read_csv('data/testset.csv')
test_X, test_y=take_dataset(test_df, PredictN)


predicted=clf.predict(test_X)
print("True target (1) in test set ", sum(test_y))
print("Predicted true target(1) in test set ",sum(predicted))
print(roc_auc_score(np.array(test_y),predicted))

#see 1st tree from the forest
tree_in_forest=clf.estimators_[0]
export_graphviz(tree_in_forest,
                feature_names=X.columns,
                filled=True,
                rounded=True)
#os.system('dot -Tpng tree.dot -o tree.png')
#os.system('dot -Tsvg tree.dot -o tree.svg')
