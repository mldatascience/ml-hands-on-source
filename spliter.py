import os
import pandas as pd
from sklearn.model_selection import train_test_split
data = pd.read_csv('data/train.csv')
tr, te= train_test_split(data, test_size=0.3)
tr.to_csv('data/trainingset.csv', index=False)
te.to_csv('data/testset.csv', index=False)
 
