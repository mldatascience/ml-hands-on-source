from sklearn import tree
import pygraphviz
import pandas as pd
import numpy as np
import os as os
data = pd.read_csv('data.csv')

Y=data.pop('target')
X = data

clf = tree.DecisionTreeClassifier(max_depth=4)
clf = clf.fit(X, Y)
#dot_data = tree.export_graphviz(clf, out_file=None)
#graph = py.source(dot_data)

tree.export_graphviz(clf,
                 feature_names=X.columns,
                 filled=True,
                 rounded=True)

os.system('dot -Tsvg tree.dot -o tree.svg')
