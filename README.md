# Message
Probably we all want to learn ML and have looked hundreds of examples of ML. I think its time to do something my ourself. To learning things we must need to put our hands on it. lets build something by ourself, even bad. I was looking a real dataset where we can apply different ML algorithms. I took a subset of dataset from a kaggle project. I think that is good to start. I did preprocess little bit for our purpose.

# Plan
We are adding a pool to choose ML algorithms that you want to apply for this dataset. I picked the problem where you can apply both regression and classification. So plans are
- take responsibility of a specific algorithm/approach,
- read and understand it
- apply it in the dataset
- present how it works, how to implement at the regular meeting

Since the dataset will be known for all, we will able to get the ML things quickly. Hopefully, we will have several implementations and we will compare different approaches. I separated training and test data so that we can compare.


# Problem description
A credit company wants to know whether they should give money to lenders based on their application data. The have historical data to understand positive or negative lenders. They provide TARGET value for each observation ("Target variable (1 - client with payment difficulties: he/she had late payment more than X days on at least one of the first Y installments of the loan in our sample, 0 - all other cases)". see details here for the dataset and column descriptions

```sh
$ head data/train.csv
$ head data/columns_description.csv
```

# ML algorithms to implement
Initially, we picked most common algorithms of ML except CNN and RNN which are might not good for this dataset. Please choose one.

- Linear Regression
- logistic Regression
- Stepwise Regression
- K-means Clustering
- Support Vector Machines
- random forest
- k-Nearest Neighbor (kNN)
- Deep Boltzmann Machine (DBM)
- Deep Neural Network

# Envionment to use
- python, pandas, numpy, tensoreflow are recommended when available
- build a docker image if you use something special... 


# General tips & instructions
- create a subfolder to keep all of your work.
- don't change anything in data folder. If you need to pre-process dataset and store then use your subfolder
- feel free to drop some columns, if you think not necessary or finds difficult to handle, etc.
- commit your work and help others to understand
- you can further split the training set to get a validation set
